package com.infiniteplugins.infinitevouchers.utils;

import com.infiniteplugins.infinitevouchers.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.configuration.ConfigurationSection;

public class ItemUtils
{
    public static ItemStack getConfigItem(final ConfigurationSection sec) {
        int data;
        try {
            data = sec.getInt("data");
        }
        catch (NumberFormatException ex) {
            Bukkit.getLogger().severe("Can not parse Material data.");
            data = 0;
        }
        Material mat = Material.getMaterial(sec.getString("material"));
        if (mat == null) {
            mat = Material.PAPER;
            Bukkit.getLogger().severe("This material is not valid, converting to paper...");
        }
        final ItemBuilder builder = new ItemBuilder(mat, data);
        builder.setName(sec.getString("name"));
        builder.setLore(sec.getStringList("lore"));
        builder.setGlow(sec.getBoolean("glow"));
        return builder.getStack();
    }
}

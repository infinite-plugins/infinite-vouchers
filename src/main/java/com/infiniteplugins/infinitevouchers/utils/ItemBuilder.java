package com.infiniteplugins.infinitevouchers.utils;

import com.infiniteplugins.infinitecore.other.HexText;
import com.infiniteplugins.infinitecore.utils.TextUtils;
import com.infiniteplugins.infinitevouchers.enums.ServerVersion;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import com.google.common.collect.Lists;
import java.util.List;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemBuilder
{
    private final ItemStack item;
    
    public ItemBuilder(final Material material, final int data) {
        this.item = new ItemStack(material, 1, (byte)data);
    }
    
    public ItemBuilder(final ItemStack item) {
        this.item = item;
    }

    public ItemBuilder setAmount(final int amount) {
        this.item.setAmount(amount);
        return this;
    }
    
    public ItemBuilder setName(final String name) {
        final ItemMeta meta = this.item.getItemMeta();
        if (ServerVersion.current().isNewerThanOrEqualTo(ServerVersion.V1_16)) {
            meta.setDisplayName(new HexText(name).translateColorCodes().parseHex().toString());
        } else if (ServerVersion.current().isOlderThanOrEqualTo(ServerVersion.v1_15)) {
            meta.setDisplayName(TextUtils.formatText(name));
        }
        this.item.setItemMeta(meta);
        return this;
    }
    
    public ItemBuilder setLore(final List<String> lore) {
        final ItemMeta meta = this.item.getItemMeta();
        ArrayList<String> lores = Lists.newArrayList();
        for (final String s : lore) {
            if (ServerVersion.current().isNewerThanOrEqualTo(ServerVersion.V1_16)) {
                 lores.add(new HexText(s).translateColorCodes().parseHex().toString());
            } else if (ServerVersion.current().isOlderThanOrEqualTo(ServerVersion.v1_15)) {
                lores.add(TextUtils.formatText(s));
            }
        }
        meta.setLore(lores);
        this.item.setItemMeta(meta);
        return this;
    }
    
    public ItemBuilder setLore(final String... lore) {
        final ItemMeta meta = this.item.getItemMeta();
        ArrayList<String> lores = Lists.newArrayList();
        for (final String s : lore) {
            if (ServerVersion.current().isNewerThanOrEqualTo(ServerVersion.V1_16)) {
                lores.add(new HexText(s).translateColorCodes().parseHex().toString());
            } else if (ServerVersion.current().isOlderThanOrEqualTo(ServerVersion.v1_15)) {
                lores.add(TextUtils.formatText(s));
            }
        }
        meta.setLore(lores);
        this.item.setItemMeta(meta);
        return this;
    }

    public ItemBuilder setGlow(boolean glow) {
        if (glow) {
            ItemMeta meta = item.getItemMeta();
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
            item.setItemMeta(meta);
        }
        return this;
    }
    
    public ItemBuilder setData(final MaterialData data) {
        final ItemMeta meta = this.item.getItemMeta();
        this.item.setData(data);
        this.item.setItemMeta(meta);
        return this;
    }
    
    public ItemStack getStack() {
        return this.item;
    }
}

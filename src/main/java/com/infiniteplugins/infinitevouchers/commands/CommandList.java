package com.infiniteplugins.infinitevouchers.commands;

import com.infiniteplugins.infinitecore.command.AbstractCommand;
import com.infiniteplugins.infinitecore.compatibility.CompatibleSounds;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import com.infiniteplugins.infinitevouchers.menu.menus.ListMenu;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandList extends AbstractCommand {

    final InfiniteVouchers instance;

    public CommandList(InfiniteVouchers instance) {
        super(false, "list");
        this.instance = instance;
    }

    @Override
    protected ReturnType runCommand(CommandSender sender, String... args) {
        new ListMenu((Player)sender);
        return ReturnType.SUCCESS;
    }

    @Override
    protected List<String> onTab(CommandSender sender, String... args) {
        return null;
    }

    @Override
    public String getPermissionNode() {
        return "infinite.vouchers.admin";
    }

    @Override
    public boolean isNoConsole() {
        return true;
    }

    @Override
    public String getSyntax() {
        return "/iv list";
    }

    @Override
    public String getDescription() {
        return "List all available vouchers.";
    }
}

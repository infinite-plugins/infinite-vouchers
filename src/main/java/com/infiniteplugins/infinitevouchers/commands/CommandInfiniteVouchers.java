package com.infiniteplugins.infinitevouchers.commands;

import com.infiniteplugins.infinitecore.command.AbstractCommand;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.List;

public class CommandInfiniteVouchers extends AbstractCommand {

    final InfiniteVouchers instance;

    public CommandInfiniteVouchers(InfiniteVouchers instance) {
        super(false, "InfiniteVouchers");
        this.instance = instance;
    }

    @Override
    protected ReturnType runCommand(CommandSender sender, String... args) {
        sender.sendMessage("");
        instance.getLocale().newMessage("&7Version &e" + instance.getDescription().getVersion()).sendPrefixedMessage(sender);

        for (AbstractCommand command : instance.getCommandManager().getAllCommands()) {
            if (command.getPermissionNode() == null || sender.hasPermission(command.getPermissionNode())) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8 - &e" + command.getSyntax() + "&8 - &7" + command.getDescription()));
            }
        }
        sender.sendMessage("");

        return ReturnType.SUCCESS;
    }

    @Override
    protected List<String> onTab(CommandSender sender, String... args) {
        return null;
    }

    @Override
    public String getPermissionNode() {
        return null;
    }

    @Override
    public String getSyntax() {
        return "/iv";
    }

    @Override
    public String getDescription() {
        return "Displays this page.";
    }
}

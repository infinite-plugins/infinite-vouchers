package com.infiniteplugins.infinitevouchers.commands;

import com.infiniteplugins.infinitecore.command.AbstractCommand;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import com.infiniteplugins.infinitevouchers.vouchers.Voucher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommandGive extends AbstractCommand {

    final InfiniteVouchers instance;
    Player target;
    ConfigurationSection sec;
    String type;
    ItemStack item;
    int amount;

    public CommandGive(InfiniteVouchers instance) {
        super(false, "give");
        this.instance = instance;
    }

    @Override
    protected ReturnType runCommand(CommandSender sender, String... args) {
        target = Bukkit.getPlayer(args[0]);
        sec = InfiniteVouchers.getInstance().getVouchersConfig().getConfigurationSection("vouchers." + args[1]);
        if (target != null) {
            if (sec != null) {
                type = args[1];
                item = new Voucher(args[1], instance).getItem();
                try {
                    amount = Integer.parseInt(args[2]);
                }

                catch (NumberFormatException ex) {
                    ex.printStackTrace();
                    String message = instance.getLocale().getMessage("command.no-number").getPrefixedMessage();
                    sender.sendMessage(message);
                    return ReturnType.SUCCESS;
                }
                item.setAmount(amount);
                target.getInventory().addItem(item);
                String giveMessage = instance.getLocale().getMessage("command.send")
                        .processPlaceholder("player", args[0])
                        .processPlaceholder("voucher", args[1])
                        .processPlaceholder("amount", String.valueOf(amount)).getPrefixedMessage();

                String receiveMessage = instance.getLocale().getMessage("command.receive")
                        .processPlaceholder("player", args[0])
                        .processPlaceholder("voucher", args[1])
                        .processPlaceholder("amount", String.valueOf(amount)).getPrefixedMessage();

                target.sendMessage(receiveMessage);
                sender.sendMessage(giveMessage);
                return ReturnType.SUCCESS;

            } else {
                String invaidVoucher = instance.getLocale().getMessage("command.no-voucher").getPrefixedMessage();
                sender.sendMessage(invaidVoucher);
                return ReturnType.SUCCESS;
            }

        } else {
            String invalidPlayer = instance.getLocale().getMessage("command.no-player").getPrefixedMessage();
                sender.sendMessage(invalidPlayer);
            return ReturnType.SUCCESS;
        }
    }

    @Override
    protected List<String> onTab(CommandSender sender, String... args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
        } else if (args.length == 2) {
            return InfiniteVouchers.getInstance().getVoucherList().stream().map(Voucher::getName).collect(Collectors.toList());
        } else if (args.length == 3) {
            return Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
        }
        return null;
    }

    @Override
    public String getPermissionNode() {
        return "infinite.vouchers.admin";
    }

    @Override
    public String getSyntax() {
        return "/iv give <player> <voucher> <amount>";
    }

    @Override
    public String getDescription() {
        return "Give a voucher to a player.";
    }
}

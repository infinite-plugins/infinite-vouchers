package com.infiniteplugins.infinitevouchers.commands;

import com.infiniteplugins.infinitecore.command.AbstractCommand;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import org.bukkit.command.CommandSender;

import java.util.List;

public class CommandReload extends AbstractCommand {

    final InfiniteVouchers instance;

    public CommandReload(InfiniteVouchers instance) {
        super(false, "reload");
        this.instance = instance;
    }

    @Override
    protected ReturnType runCommand(CommandSender sender, String... args) {
        instance.reloadConfig();
        instance.getLocale().getMessage("&7Configuration and Language files reloaded.").sendPrefixedMessage(sender);
        return ReturnType.SUCCESS;
    }

    @Override
    protected List<String> onTab(CommandSender sender, String... args) {
        return null;
    }

    @Override
    public String getPermissionNode() {
        return "infinite.vouchers.admin";
    }

    @Override
    public String getSyntax() {
        return "/iv reload";
    }

    @Override
    public String getDescription() {
        return "Reload the Configuration and Language files.";
    }
}

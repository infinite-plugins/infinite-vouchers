package com.infiniteplugins.infinitevouchers.settings;

import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitecore.config.ConfigSection;
import com.infiniteplugins.infinitecore.config.ConfigSetting;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import org.bukkit.event.Listener;

public class Settings implements Listener {

    static final Config config = InfiniteVouchers.getInstance().getCoreConfig();

    public static final ConfigSetting LANGUGE_MODE = new ConfigSetting(config, "system.language", "en_US",
            "The enabled language file.",
            "More language files (if available) can be found in the plugins data folder.");

    public static void setupConfig() {
        config.load();
        config.setAutoremove(true).setAutosave(true);
        config.saveChanges();
    }
}
package com.infiniteplugins.infinitevouchers.enums;

import org.bukkit.Bukkit;

public enum ServerVersion
{
    v1_7(true, false),
    v1_8(true),
    v1_9(true),
    v1_10(true),
    v1_11(true),
    v1_12(true),
    v1_13(false),
    v1_14(false),
    v1_15(false),
    V1_16(false),
    UNKNOWN(false, false);

    private final boolean legacy;
    private boolean fullySupported;
    private static ServerVersion currentVersion;
    private static String version;

    public boolean isNewerThanOrEqualTo(final ServerVersion version) {
        return this.ordinal() >= version.ordinal();
    }

    public boolean isOlderThanOrEqualTo(final ServerVersion version) {
        return this.ordinal() <= version.ordinal();
    }

    public static ServerVersion current() {
        if (ServerVersion.currentVersion != null) {
            return ServerVersion.currentVersion;
        }
        ServerVersion.version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        for (final ServerVersion serverVersion : values()) {
            if (ServerVersion.version.contains(serverVersion.name())) {
                ServerVersion.currentVersion = serverVersion;
                break;
            }
        }
        if (ServerVersion.currentVersion == null) {
            ServerVersion.currentVersion = ServerVersion.UNKNOWN;
        }
        return ServerVersion.currentVersion;
    }

    public String getExactVersion() {
        if (ServerVersion.currentVersion == null) {
            current();
        }
        return ServerVersion.version;
    }

    ServerVersion(final boolean legacy) {
        this.fullySupported = true;
        this.legacy = legacy;
    }

    ServerVersion(final boolean legacy, final boolean fullySupported) {
        this.fullySupported = true;
        this.legacy = legacy;
        this.fullySupported = fullySupported;
    }

    public boolean isLegacy() {
        return this.legacy;
    }

    public boolean isFullySupported() {
        return this.fullySupported;
    }
}


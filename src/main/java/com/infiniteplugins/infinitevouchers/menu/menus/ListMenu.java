package com.infiniteplugins.infinitevouchers.menu.menus;

import java.util.List;

import com.infiniteplugins.infinitecore.compatibility.CompatibleSounds;
import com.infiniteplugins.infinitecore.menus.*;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import com.infiniteplugins.infinitevouchers.menu.Menu;
import com.infiniteplugins.infinitevouchers.menu.MenuAPI;
import com.infiniteplugins.infinitevouchers.vouchers.Voucher;
import com.infiniteplugins.infinitevouchers.utils.ItemUtils;
import com.google.common.collect.Lists;
import org.bukkit.inventory.ItemStack;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class ListMenu extends CustomMenu {
    public ListMenu(final Player p) {
        super(InfiniteVouchers.getInstance().getMenuConfig().getString("list-menu.title"), InfiniteVouchers.getInstance().getMenuConfig().getInt("list-menu.size"));
        final ConfigurationSection sec = InfiniteVouchers.getInstance().getMenuConfig().getConfigurationSection("list-menu");
        for (int i = 0; i < this.getSize(); ++i) {
            this.getMenu().addMenuItem(new com.infiniteplugins.infinitevouchers.menu.MenuItem() {
                @Override
                public void onClick(final Player player, final InventoryClickType inventoryClickType) {
                }
                
                @Override
                public ItemStack getItemStack() {
                    return ItemUtils.getConfigItem(sec.getConfigurationSection("fill-item"));
                }
            }, i);
        }
        final List<com.infiniteplugins.infinitevouchers.menu.MenuItem> items = Lists.newArrayList();
        InfiniteVouchers.getInstance().getVoucherList().forEach(voucher -> items.add(new com.infiniteplugins.infinitevouchers.menu.MenuItem() {



            @Override
            public void onClick(final Player player, final InventoryClickType inventoryClickType) {
                player.getInventory().addItem(voucher.getItem());
                CompatibleSounds.ENTITY_PLAYER_LEVELUP.play(p);
            }

            @Override
            public ItemStack getItemStack() {
                return voucher.getItem();
            }
        }));
        this.getMenu().setupPages(items, sec.getIntegerList("voucher-slots"));
        this.getMenu().addMenuItem(new com.infiniteplugins.infinitevouchers.menu.MenuItem() {
            @Override
            public void onClick(final Player player, final InventoryClickType inventoryClickType) {
                if (ListMenu.this.getMenu().getCurrentPage() == 1) {
                    CompatibleSounds.BLOCK_ANVIL_LAND.play(p);
                    this.setTemporaryIcon(ItemUtils.getConfigItem(sec.getConfigurationSection("invalid-page")), 5L);
                    return;
                }
                CompatibleSounds.ENTITY_CHICKEN_EGG.play(p);
                ListMenu.this.getMenu().previousPage(p);
            }
            
            @Override
            public ItemStack getItemStack() {
                return ItemUtils.getConfigItem(sec.getConfigurationSection("previous-page"));
            }
        }, sec.getConfigurationSection("previous-page").getInt("slot"));
        this.getMenu().addMenuItem(new com.infiniteplugins.infinitevouchers.menu.MenuItem() {
            @Override
            public void onClick(final Player player, final InventoryClickType inventoryClickType) {
                if (ListMenu.this.getMenu().getMaxPage() == ListMenu.this.getMenu().getCurrentPage()) {
                    CompatibleSounds.BLOCK_ANVIL_LAND.play(p);
                    this.setTemporaryIcon(ItemUtils.getConfigItem(sec.getConfigurationSection("invalid-page")), 30L);
                    return;
                }
                CompatibleSounds.ENTITY_CHICKEN_EGG.play(p);
                ListMenu.this.getMenu().nextPage(p);
            }
            
            @Override
            public ItemStack getItemStack() {
                return ItemUtils.getConfigItem(sec.getConfigurationSection("next-page"));
            }
        }, sec.getConfigurationSection("next-page").getInt("slot"));
        this.getMenu().setMenuCloseBehaviour(new MenuAPI.MenuCloseBehaviour() {
            @Override
            public void onClose(final Player player, final Menu menu, final boolean b) {
                if (!b) {
                    CompatibleSounds.BLOCK_CHEST_CLOSE.play(p);
                }
            }
        });
        this.getMenu().openMenu(p);
    }
}

package com.infiniteplugins.infinitevouchers.menu.menus;

import com.infiniteplugins.infinitecore.utils.TextUtils;
import com.infiniteplugins.infinitevouchers.menu.Menu;
import com.infiniteplugins.infinitevouchers.menu.MenuAPI;
import org.bukkit.entity.Player;

public class CustomMenu
{
    private Menu menu;
    private int size;

    public CustomMenu(final String title, final int size) {
        this.menu = MenuAPI.getInstance().createMenu(TextUtils.formatText(title), size / 9);
        this.size = size;
    }

    public int getSize() {
        return this.size;
    }

    public Menu getMenu() {
        return this.menu;
    }

    public void openMenu(final Player p) {
        this.menu.openMenu(p);
    }
}

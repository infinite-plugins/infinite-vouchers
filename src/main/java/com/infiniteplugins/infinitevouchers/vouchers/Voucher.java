package com.infiniteplugins.infinitevouchers.vouchers;

import com.infiniteplugins.infinitecore.compatibility.CompatibleSounds;
import com.infiniteplugins.infinitecore.other.HexText;
import com.infiniteplugins.infinitecore.utils.TextUtils;
import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import com.infiniteplugins.infinitevouchers.enums.ServerVersion;
import com.infiniteplugins.infinitevouchers.listeners.PlayerCommandListener;
import com.infiniteplugins.infinitevouchers.utils.ItemUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.configuration.ConfigurationSection;
import java.util.List;

import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Voucher
{
    private String name;
    private ItemStack item;
    private List<String> commandList;
    private ConfigurationSection sec;
    private boolean announcement;
    private boolean message;
    private boolean sound;
    private int data;
    boolean glow;
    private final InfiniteVouchers instance;

    public Voucher(final String name, InfiniteVouchers instance) {
        this.name = name;
        this.sec = InfiniteVouchers.getInstance().getVouchersConfig().getConfigurationSection("vouchers." + name);
        this.instance = instance;
        this.item = ItemUtils.getConfigItem(this.sec.getConfigurationSection("item"));
        data = sec.getInt("data");
        this.commandList = (List<String>)this.sec.getStringList("commands");
        this.announcement = this.sec.getBoolean("broadcast.enabled");
        this.message = this.sec.getBoolean("message.enabled");
        this.sound = this.sec.getBoolean("sound.enabled");
        this.glow = this.sec.getBoolean("glow");
    }
    
    public ItemStack getItem() {
        return this.item;
    }
    
    public String getName() {
        return this.name;
    }

    public boolean getGlow() {
        return this.glow;
    }

    public void handleCommands(final Player player) {
        for (String command : this.commandList) {
            command = command.replaceAll("%player%", player.getName());
            if (command.startsWith("[delay")) {
                String delayCommand = StringUtils.substringBetween(command, "[", "]");
                int delay = Integer.parseInt(delayCommand.split("-", 2)[1]);
                final String finalCommand = command.replace("[" + delayCommand + "]", "");
                final ItemStack heldItem = item;
                Bukkit.getScheduler().scheduleSyncDelayedTask(instance, () -> {
                    runCommand(finalCommand, player);
                }, 20 * delay);
            } else if (command.startsWith("[op]")) {
                command = command.replace("[op]", "");
                boolean wasOp = player.isOp();
                PlayerCommandListener.addCommand(player.getUniqueId(), command);
                player.setOp(true);
                player.performCommand(command);

                if (!wasOp) {
                    player.setOp(false);
                }

                PlayerCommandListener.removeCommand(player.getUniqueId());
            } else {
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
            }
        }
    }

    private void runCommand(String command, Player player) {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
    }


    public void getMessage(final Player p) {
        if (this.message) {
            for (String msg : this.sec.getStringList("message.message")) {
                if (ServerVersion.current().isNewerThanOrEqualTo(ServerVersion.V1_16)) {
                    msg = new HexText(msg).translateColorCodes().parseHex().toString();
                } else {
                    msg = TextUtils.formatText(msg);
                }
                msg = msg.replaceAll("%type%", this.name);
                p.sendMessage(msg);
            }
        }
    }
    
    public void sendAnnouncement(final Player redeemer) {
        if (this.announcement) {
            for (String msg : this.sec.getStringList("broadcast.message")) {
                if (ServerVersion.current().isNewerThanOrEqualTo(ServerVersion.V1_16)) {
                    msg = new HexText(msg).translateColorCodes().parseHex().toString();
                } else {
                    msg = TextUtils.formatText(msg);
                }
                msg = msg.replaceAll("%player%", redeemer.getName());
                msg = msg.replaceAll("%type%", this.name);
                Bukkit.broadcastMessage(msg);
            }
            for (final Player plrs : Bukkit.getOnlinePlayers()) {
                CompatibleSounds.BLOCK_CHEST_CLOSE.play(plrs);
            }
        }
    }
    
    public void playSound(final Player p) {
        if (this.sound) {
            p.playSound(p.getLocation(), CompatibleSounds.valueOf(this.sec.getString("sound.name").toUpperCase()).getSound(), 1, 1);
        }
    }

    public void addGlow() {
        if (glow) {
            ItemMeta meta = item.getItemMeta();
            meta.addEnchant(Enchantment.DURABILITY, 1, false);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
    }
}

package com.infiniteplugins.infinitevouchers.listeners;

import com.infiniteplugins.infinitevouchers.InfiniteVouchers;
import com.infiniteplugins.infinitevouchers.vouchers.Voucher;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.Listener;

public class PlayerInteractListener implements Listener {


    @EventHandler
    public void voucherListener(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final ItemStack current = player.getItemInHand();
        final ItemStack offhand = player.getInventory().getItemInOffHand();
        if ((event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName() && event.getItem().getItemMeta().hasLore()) {
            Voucher vo = null;

            for (Voucher voucher : InfiniteVouchers.getInstance().getVoucherList()) {
                if (voucher.getItem().isSimilar(event.getItem())) {
                    vo = voucher;
                    break;
                }
            }

            if (vo != null) {
                event.setCancelled(true);
                if (current.getAmount() == 1) {
                    player.setItemInHand((ItemStack)null);
                } else if (offhand.getAmount() == 1) {
                    player.getInventory().setItemInOffHand(null);
                } else if (player.getItemInHand().getAmount() > 1) {
                    current.setAmount(current.getAmount() - 1);
                } else {
                    offhand.setAmount(offhand.getAmount() - 1);
                }

                vo.handleCommands(event.getPlayer());
                vo.getMessage(event.getPlayer());
                vo.sendAnnouncement(event.getPlayer());
                vo.playSound(event.getPlayer());
            }
        }
    }
}


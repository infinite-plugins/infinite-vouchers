package com.infiniteplugins.infinitevouchers;

import com.google.common.collect.Lists;
import com.infiniteplugins.infinitecore.InfiniteCore;
import com.infiniteplugins.infinitecore.InfinitePlugin;
import com.infiniteplugins.infinitecore.command.CommandManager;
import com.infiniteplugins.infinitecore.compatibility.CompatibleMaterial;
import com.infiniteplugins.infinitecore.config.Config;
import com.infiniteplugins.infinitecore.messages.Locale;
import com.infiniteplugins.infinitevouchers.commands.*;
import com.infiniteplugins.infinitevouchers.enums.ServerVersion;
import com.infiniteplugins.infinitevouchers.listeners.PlayerInteractListener;
import com.infiniteplugins.infinitevouchers.listeners.PlayerCommandListener;
import com.infiniteplugins.infinitevouchers.menu.MenuAPI;
import com.infiniteplugins.infinitevouchers.vouchers.Voucher;
import com.infiniteplugins.infinitevouchers.settings.Settings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class InfiniteVouchers extends InfinitePlugin {

    private static InfiniteVouchers instance;
    private List<Voucher> voucherList;
    private CommandManager commandManager;
    private Config vouchersConfig = new Config(this, "vouchers.yml");
    private Config menuConfig = new Config(this, "menus.yml");

    @Override
    public void onPluginLoad() {
        instance = this;
    }

    @Override
    public void onPluginEnable() {
        InfiniteCore.registerPlugin(this, 6, CompatibleMaterial.PAPER);

        ServerVersion.current();

        // Setup Config
        Settings.setupConfig();
        Locale.loadDefaultLocale(this, "nl_NL");
        this.setLocale(Settings.LANGUGE_MODE.getString(), false);

        // Register commands
        this.commandManager = new CommandManager(this);
        this.commandManager.addCommand(new CommandInfiniteVouchers(this))
                .addSubCommands(
                        new CommandGive(this),
                        new CommandList(this),
                        new CommandReload(this)
                );

        // Setup Extra Configs
        if (!new File(this.getDataFolder(), "vouchers.yml").exists()) {
            saveResource("vouchers.yml", false);
        }
        if (!new File(this.getDataFolder(), "menus.yml").exists()) {
            saveResource("menus.yml", false);
        }
        vouchersConfig.load();
        menuConfig.load();
        voucherList = Lists.newArrayList();
        loadVouchers();

        //Register the Events
        PluginManager manager = Bukkit.getServer().getPluginManager();
        manager.registerEvents(new PlayerInteractListener(), this);
        manager.registerEvents(MenuAPI.getInstance(), this);
        manager.registerEvents(new PlayerCommandListener() ,this);

        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eInfinite Vouchers " + instance.getDescription().getVersion()));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Infinite Vouchers has been &eEnabled"));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));


    }

    @Override
    public void onPluginDisable() {

        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eInfinite Vouchers " + instance.getDescription().getVersion()));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eDisabling &7Infinite Vouchers..."));
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));

    }


    @Override
    public void onConfigReload() {
        vouchersConfig.load();
        menuConfig.load();
        voucherList = Lists.newArrayList();
        loadVouchers();
        this.setLocale(getConfig().getString("system.language"), true);
        this.locale.reloadMessages();
    }

    @Override
    public List<Config> getExtraConfig() {
        return Collections.singletonList(vouchersConfig);
    }



    public void loadVouchers() {
        if (!this.voucherList.isEmpty()) {
            this.voucherList.clear();
        }
        for (final String key : this.getVouchersConfig().getConfigurationSection("vouchers").getKeys(false)) {
            this.voucherList.add(new Voucher(key, instance));
        }
    }
    public List<Voucher> getVoucherList() {
        return this.voucherList;
    }

    public static InfiniteVouchers getInstance() {
        return instance;
    }

    public Config getVouchersConfig() {
        return this.vouchersConfig;
    }

    public Config getMenuConfig() {
        return menuConfig;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }


}